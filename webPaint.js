//2019spring Software Stutio, webPaint Canvas 
//author: JKao, CS NTHU

var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');
let status  = 0;
const drawing  = 0;
const shaping = 1;
const texting  = 2;
const erasing = 3;

let fontSize = 15;
let fontType = 0;

let TextX = -100;
let TextY = -100;

let shape = 0;
const rectangle = 5;
const triangle = 6;
const circle = 7;
var rec = 0, cir = 0, tri = 0;

let isMousedown = false;
let israinbow = false;

//return mouse postion on canvas
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

//draw by mouse moving
function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);

  if(status == drawing){
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    ctx.globalCompositeOperation = "source-over";
    if(isMousedown == true && israinbow == true){
      rainbow();
      //console.log(hue);
    }
  }
  else if(status == texting){
    ctx.globalCompositeOperation="source-over";
    ctx.fillStyle = "rgba(10,10,10)";
    if(rec == 0){
        cx = mousePos.x;
        cy = mousePos.y;
        rec = 1;
        TextX = cx+15;

        if(fontSize == 15){
            ctx.fillRect(cx,cy,150,30);
            ctx.clearRect(cx+5,cy+5,140,20);
            TextY = cy+15;
        }
        else if(fontSize == 32){
            ctx.fillRect(cx,cy,200,60);
            ctx.clearRect(cx+10,cy+10,180,40);
            TextY = cy+32;
        }
        else if(fontSize == 50){
            ctx.fillRect(cx,cy,250,80);
            ctx.clearRect(cx+15,cy+15,220,50);
            TextY = cy+50;
        }
    }
  }
  else if(status == shaping){
    if(shape == rectangle){
      if(rec == 0){
        cx = mousePos.x;
        cy = mousePos.y;
        rec = 1;
      }
      if(rec == 1){
        ctx.fillRect(cx,cy,mousePos.x-cx,mousePos.y-cy);
      }
    }
    else if(shape == triangle){
      if(tri == 0){
        cx = mousePos.x;
        cy = mousePos.y;
        ctx.moveTo(cx,cy);
        tri = 1;
      }
      if(tri == 1){
        ctx.moveTo(cx,cy);
        ctx.lineTo(2*cx-mousePos.x,mousePos.y);
        ctx.lineTo(mousePos.x,mousePos.y);
        ctx.fill();
      }
    }
    else if(shape == circle){
      if(cir == 0){
        cx = mousePos.x;
        cy = mousePos.y;
        cir = 1;
      }
      if(cir == 1){
        ctx.arc(cx,cy, Math.abs(mousePos.x-cx)/2,0,Math.PI*2);
        ctx.fill();
      }
    }

    if(isMousedown == true && israinbow == true){
      rainbow();
    }
  }  
  else if(status == erasing){
    ctx.lineTo(mousePos.x, mousePos.y);
    if(isMousedown == true){
      ctx.globalCompositeOperation = "destination-out";
      ctx.stroke();  
    }
  }
}

//start listening to mouse moving 
canvas.addEventListener('mousedown', function(evt) {
  isMousedown = true;
  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.moveTo(mousePos.x, mousePos.y);
  evt.preventDefault();
  canvas.addEventListener('mousemove', mouseMove, false);
});

//redo & undo function
let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

window.addEventListener('popstate', changeStep, false);

function changeStep(e){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if( e.state ){
    ctx.putImageData(e.state, 0, 0);
  }
}


//stop listening to mouse
canvas.addEventListener('mouseup', function() {
  isMousedown = false;
  rec = 0;
  cir = 0;
  tri = 0 ;
  var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);

  canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

//reset to clear canvas
document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

var colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink', 'black', 'white', 'rainbow'];
var lineWidth = [1, 3, 5, 10, 15, 20];
var lineWidthType = ['default', 'three', 'five', 'ten', 'fifteen', 'twenty'];
var functionType = ['drawing', 'text', 'eraser', 'rectangle', 'triangle', 'circle', 'undo', 'redo',];

//---------Function Status Setting---------//
//change function or shape by button
function setfunction(i) {
  document.getElementById(functionType[i]).addEventListener('click', function() {
    console.log(functionType[i]);

    if(functionType[i]=='eraser'){
      status = erasing;
      document.body.style.cursor = 'cell';
    }
    else if(functionType[i] == 'drawing'){
      status = drawing;
      document.body.style.cursor = 'crosshair';
    }
    else if(functionType[i] == 'text'){
      status = texting;
    }
    else if(functionType[i] == 'undo'){
      history.go(-1);
    }
    else if(functionType[i] == 'redo'){
      history.go(1);
    }
    else {
      status = shaping;
      document.body.style.cursor = 'grabbing';
      if(functionType[i]=='rectangle')
        shape = rectangle;
      else if(functionType[i] == 'triangle')
        shape = triangle;
      else if(functionType[i]== 'circle')
        shape = circle;
      else  
        shape = 0;
    }

  }, false);
}


//---------drawing Setting---------//
//change color by button
function colorType(i) {
  document.getElementById(colors[i]).addEventListener('click', function() {
    //status = drawing;
    if(colors[i] == 'rainbow'){
      israinbow = true;
    }
    else{
      israinbow = false;
      ctx.strokeStyle = colors[i];
      ctx.fillStyle = colors[i];
    }
    console.log(colors[i]);
  }, false);
}

//change color by rainbow 
let hue = 0;
function rainbow(){
  ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
  ctx.fillStyle = `hsl(${hue}, 100%, 50%)`;
  hue++; // 色相環 度數更新
  if (hue >= 360) {
      hue = 0;
  }
}

//change lineWidth by button
function setlineWidth(i) {
  document.getElementById(lineWidthType[i]).addEventListener('click', function() {
    ctx.lineWidth = lineWidth[i];
    console.log(lineWidth[i]);
  }, false);
}

//---------Font Setting---------//
//set fontSize
function setfontsize(size) {
  fontSize = size;
  if(fontType == 0)
    ctx.font = size +'px Arial';   
  else if(fontType == 1)   
    ctx.font = size +'px serif';
  else 
    ctx.font = size +'px Courier New';
}

//set fontType
function setfontType(type) {
  fontType = type;
  if(type == 0)
    ctx.font = fontSize +'px Arial'; 
  else if(type == 1)
    ctx.font = fontSize +'px Serif';
  else
    ctx.font = fontSize +'px Courier New';
}

document.getElementById('textboard').addEventListener('keypress', function(event) {
  if (event.keyCode == 13 ) {
      ctx.fillText(document.getElementById('textboard').value, TextX, TextY);
      ctx.textAlign = 'left';
      document.getElementById('textboard').value="";
  }
});


//---------btn click Setting---------//
//keep listening mouseclick if color and font button
for(var i = 0; i < colors.length; i++) {
  colorType(i);
}

for(var i = 0; i < lineWidth.length; i++) {
  setlineWidth(i);
}

for(var i = 0; i < functionType.length; i++) {
  setfunction(i);
}


//download
var download = document.getElementById('btn_download');
download.addEventListener('click', function(){
  var dataURL = canvas.toDataURL('image/webPaint.png');
  this.href = dataURL;
  console.log('download succeed');
})

//upload
var imageLoader = document.getElementById('btn_upload');
imageLoader.addEventListener('change', handleImage, false);
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}