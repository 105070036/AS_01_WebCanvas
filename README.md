# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/04 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

進入web canvas的畫面後，中間空白部分為作畫區域，左右兩邊為功能區。以下進行說明:

Basic components
1.  左上方有明顯的reset按鈕，用作整張canvas的清除。
2.  左方有多種的顏色方格區，用來自由設定畫筆、文字、以及圖型的顏色。
3.  左方的六格數字方格區，用來自由設定畫筆線條粗細。  
4.  左方的畫筆、T字樣、以及橡皮擦圖樣，各自代表其功能。
5.  T字樣按鈕用來在canvas輸入文字，每次使用時將游標放在畫布上點擊移動，會出現文字預計呈現的位置，接著選擇右上方的文字大小、字型設定，並且輸入文字於一旁的方框內，按下Enter即可呈現。
5.  橡皮擦用來把所經地區變成透明(意即清除)，可以選取調整線條粗細，以畫筆的方式清除，另外也可以使用圖型(矩形、三角形、圓形)來控制橡皮擦的清除範圍，進行區域清除。
6.  游標在進行不同功能、或移動到不同區域時會有所改變，譬如作畫、選取左方、右方的功能、點擊等等。

Advance tools
1.  右方的圖型(矩形、三角形、圓形)透過滑鼠點下，可以在畫布上點擊拖曳的方式決定大小範圍，並且也能搭配左方顏色表做變化。
2.  Undo/Redo的功能置於canvas右方，意即復原/取消復原。
3.  download目前canvas圖片檔的功能置於左下方，透過滑鼠點擊使用。
4.  upload圖片的功能置於左下方功能列，點擊後選取圖片上傳到畫布上。

Other useful widgets
1.  左方的顏色方格區中，有特別標註"Rainbow"字樣的按鈕，可以搭配畫筆、圖型來實作出彩虹般的顏色呈現。

Note:   基本上所有功能實作的地方，都能依照在.js檔案裡面的註解找到相對應的寫法